$.fn.tabCustom = function (option) {
    var _default = $.extend({
        click : '[data-tab-role=click]',
        section : '[data-tab-role=section]',
        group : '[data-tab-role=group]',
        duration : 500,
        prevHide : true,
        type: 'tab',
        activeClass : 'active',
        response : false,
        callback : false
    }, option);
    var wrap = $(this), delay, wv = 0;
    var section = $(this).find(_default.section);

    var core = {
        tabClick : false,
        tabLayout : function () {
            if(wv == $(window).width()){
                return false
            }
            wv = $(window).width();
            wrap.find('.tab-section-button.active').removeClass('active');
            max = wrap.removeClass('min')[0].clientWidth;
            count = 0;
            for(i=0;i<this.tabClick.length;i++){
                mRight = parseInt($(this.tabClick[i]).css('margin-right').replace('px', ''));
                mLeft = parseInt($(this.tabClick[i]).css('margin-left').replace('px', ''));
                if(max < ( count += ($(this.tabClick[i]).outerWidth(true) + (mLeft + mRight) ) ) ){
                   wrap.addClass('min');
                   break;
                }
            }
            if(!wrap.hasClass('min')){
                for(j=0;j<core.tabClick.length;j++){
                    if($(core.tabClick[j]).hasClass('active')){
                        $(section[j]).addClass('active')[0].style.display = 'block';
                        break;
                    }
                }
            }else{
                for(j=0;j<section.length;j++){
                    $(section[j]).removeClass(_default.activeClass)[0].style.display = 'none';
                }
            }
        },
        tabChange: function () {
            click = $(this);
            index = $(this).attr('section');
            section.each(function () {
                if($(this).is(":visible") && $(this).attr('data-section') != index){
                    $('[section='+$(this).removeClass(_default.activeClass).css({'display':'none'}).attr('data-section')+']').removeClass(_default.activeClass);
                    if(_default.callback){
                        _default.callback('change', 'off', $(this))
                    }
                }else if(index == $(this).attr('data-section')){
                    if($(this).is(":visible")){
                        $(this).removeClass(_default.activeClass)[0].style.display = 'none';
                        click.removeClass(_default.activeClass);
                    }else{
                        $(this).addClass(_default.activeClass).velocity('fadeIn', 350);
                        click.addClass(_default.activeClass);
                        if(_default.callback){
                            _default.callback('change', 'on', $(this))
                        }
                    }
                }
            })
        },
        tab : function () {
            this.tabClick = wrap.find(_default.click).each(function (i, index) {
                key = Math.random().toString(36).substring(7);
                $(this).attr('section', key);
                if(section[i] != undefined) {
                    $(section[i]).attr('data-section', key);
                    if (_default.response) {
                        $(section[i]).before('<div class="tab-section-button" section="' + key + '">' + $(this).html() + '</div>')
                    }
                    if (i == 0) {
                        $(this).addClass(_default.activeClass);
                        $(section[i]).addClass(_default.activeClass);
                    } else {
                        section[i].style.display = 'none';
                    }
                }
            });

            wrap.find('.tab-section-button, '+_default.click).on('touched click', core.tabChange);

            if(_default.response) {
                this.tabLayout();

                window.addEventListener('resize', function () {
                    clearTimeout(delay);
                    delay = setTimeout(function () {
                        core.tabLayout();
                    }, 300)
                }, false);
            }
        },
        accordion : function () {
            wrap.find(_default.click).on('touched click', function () {
                group = $(this).closest(_default.group);
                current = group.find(_default.section);
                self = $(this);

                section.each(function () {
                    if(!$(this).is(current) && $(this).is(":visible")){
                        if(_default.prevHide){
                            $(this).velocity('slideUp', _default.duration).closest(_default.group)
                                .removeClass(_default.activeClass).find(_default.click).removeClass(_default.activeClass)
                        }
                    }else if($(this).is(current)){
                        if($(this).is(":visible")){
                            current.velocity('slideUp', _default.duration);
                            group.removeClass(_default.activeClass);
                            self.removeClass(_default.activeClass);
                            if(_default.callback){
                                _default.callback('change', 'off', current)
                            }
                        }else{
                            current.velocity('slideDown', _default.duration);
                            group.addClass(_default.activeClass);
                            self.addClass(_default.activeClass);
                            if(_default.callback){
                                _default.callback('change', 'on', current)
                            }
                        }
                    }
                })
            })
        }
    }

    if(_default.type == 'tab'){
        core.tab();
    }else{
        core.accordion();
    }
}