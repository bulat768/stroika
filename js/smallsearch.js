$.fn.smallSearch = function (option) {
    var config = $.extend({
        callback : false
    }, option);

    var form = $(this);
    var button = $(this).find('[data-search=button]') || false;
    var field = $(this).find('[data-search=control]') || false;
    var dropdown = $(this).find('[data-search=dropdown]') || false;

    function submit() {
        // тут общий сабмит для формы
        console.log('submit', form)
    }

    if(button){
        button.on('touched click', function () {
            if(field.val() != ''){
                submit();
            }
        });
    }
    if(field){
        field.on('click touched keyup write paste', function(e) {
            val = $(this).val();
            if(val.length > 3){
                console.log(val, 'debug');
                // тут запрос по значению
                // тестовый пример
                $.ajax({
                    type : 'POST',
                    url : field.attr('data-url'),
                    success: function (result) {
                        if(result){
                            dropdown.html(result).fadeIn(300)
                        }else{
                            dropdown.fadeOut(300);
                        }
                    }
                })
            }else{
                dropdown.fadeOut(300);
            }
        }).trigger('write');

        $(document).on('touched click', function(e){
            if (!dropdown.is(e.target) && dropdown.has(e.target).length === 0) {
                if(dropdown.is(":visible")) {
                    field.val('');
                    dropdown.fadeOut(300);
                }
            }
        });
    }
}