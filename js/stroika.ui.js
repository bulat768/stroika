
(function( $ ) {
    $.fn.exists = function(callback) {
        var args = [].slice.call(arguments, 1);
        if (this.length && callback) {
            callback.call(this, args);
        }
        return this;
    };


    $.fn.fxGrid = function () {
        var grid = $(this), timer;

        function run(e){
            grid.each(function () {
                col = Math.floor(this.clientWidth / parseInt( ($(this).attr('data-min-col') || 200) ));
                maxCol =  parseInt($(this).attr('data-max-col') || 5);
                if(col <= 0){
                    col = 1;
                }
                $(this).children().css({'width': (100 / ( col > maxCol ? maxCol : col )) +'%'});
            });
        }

        run();

        window.addEventListener('resize', function () {
            clearTimeout(timer);
            timer = setTimeout(function () {
                run();
            }, 300)
        });

        $(window).on('load', function () {
            run();
        })
    }


    $.fn.topBar = function () {
        var bar = $(this);
        function close() {
            $('html').removeClass('top-bar-open');
        }
        var mask = $('<div>', {
            'class':'top-bar-mask'
        }).on('touched click', close);
        $(document).on('top-bar-close', close);

        var close = $('<div>',{
            'class':'top-bar-close icon-close'
        }).on('touched click', close);

        $(document).on('tocuhed click', '[data-top-bar=toggle]', function () {
            $('html').toggleClass('top-bar-open');
        });

        $('body').append(mask);
        bar.append(close);

    }
    $.fn.menu = function () {
        var menu = $(this).addClass('running'),
            wv = window.innerWidth,
            update,
            delay,
            clone = [],
            header,
            toggle,
            popup;
        var toggleDelay;
        function viewport() {
            if(window.scrollY < menu.offset().top){
                return ($(window).height() - (menu.offset().top - window.scrollY)) - menu[0].clientHeight;
            }else{
                return $(window).height() - menu[0].clientHeight;
            }
        }

        var dropdown = {
            element : false,
            close : function () {
                toggle.removeClass('in-active');
                $('html').removeClass('menu-dropdown-active')
                this.element.slideUp(300);
            },
            open : function () {
                toggle.addClass('in-active');
                $('html').addClass('menu-dropdown-active');
                this.element.slideDown(300).find('.menu-dropdown-overflow').css({
                    'max-height':  viewport()+'px'
                })
            },
            change : function () {
                if(this.element.is(":visible")){
                    this.close();
                }else{
                    this.open();
                }
            }
        };

        function device() {
            if(window.innerWidth < 860){
                return 'mobile';
            }
            if(!header){
                return 'mobile';
            }
            menu.removeClass('mobile')
            var horizontal = header.find('.menu-button');
            var max = menu[0].clientWidth+2;
            var count = 0;
            for(i=0;i<horizontal.length;i++){
                if(max < (count+= horizontal[i].clientWidth ) ){
                    return 'mobile';
                    break;
                }
            }
            return 'desktop';
        }

        var ui = {
            mouseover : function () {
                clearTimeout(delay);
                $(this).addClass('hover');
                delay = setTimeout(function () {
                    dropdown.element.find('.menu-category-item.active').removeClass('active');
                    popup.addClass('on').children().eq(
                        parseInt(dropdown.element.find('.menu-category-item.hover').removeClass('hover').addClass('active').attr('data-href-index'))
                    ).fadeIn(300).siblings().css({'display':'none'})
                }, 300);
            },
            mouseleaveClose : function () {
                dropdown.element.find('.menu-category-item.active').removeClass('active');
                popup.removeClass('on').children().removeClass('active');
            },
            mouseleave: function () {
                $(this).removeClass('hover');
                clearTimeout(delay);
                delay = setTimeout(function () {
                    ui.mouseleaveClose();
                }, 300);
            },
            click : function () {
                div = $('#'+this.getAttribute('data-href'));
                if(div.is(":visible")){
                    div.slideUp(400)
                }else{
                    div.slideDown(400)
                }
            }
        };

        var toogleUi = {
            click : function () {
                dropdown.change();
            },
            mouseover : function () {
                clearTimeout(toggleDelay);
                toggleDelay = setTimeout(function () {
                    if(!dropdown.element.is(":visible")){
                        dropdown.open();
                    }
                }, 200);
            },
            mouseleave : function () {
                clearTimeout(toggleDelay);
                toggleDelay = setTimeout(function () {
                    if(dropdown.element.is(":visible")){
                        dropdown.close();
                    }
                }, 300)
            },
            clear : function () {
                clearTimeout(toggleDelay);
            }
        }


        function layout() {
            dropdown.category.attr('style', '');
            dropdown.category.find('.menu-category-data').attr('style', '');
            if(!dropdown.category && !dropdown.popup){
                return false;
            }else if(device() == 'desktop'){
                if(toggle){
                    toggle.find('span').html(toggle.attr('data-full-title'));

                    toggle[0].addEventListener('mouseover', toogleUi.mouseover);
                    toggle[0].addEventListener('mouseleave', toogleUi.mouseleave);
                    toggle[0].removeEventListener('click', toogleUi.click);

                    if(toggle.attr('href') == '' || !toggle.attr('href')){
                        toggle.attr('href', toggle.attr('data-href'));
                    }
                }
                if(dropdown.category){
                    dropdown.category.find('[data-href]').each(function(){
                        $(this)[0].removeEventListener('click', ui.click);
                        $(this)[0].addEventListener('mouseover', ui.mouseover);
                        $(this)[0].addEventListener('mouseleave', ui.mouseleave);
                    });
                    popup[0].addEventListener('mouseover', function () {
                        clearTimeout(delay);
                    });
                    popup[0].addEventListener('mouseleave', ui.mouseleave);

                    dropdown.element[0].addEventListener('mouseover', toogleUi.clear);
                    dropdown.element[0].addEventListener('mouseleave', toogleUi.mouseleave);
                }
            }else{
                menu.addClass('mobile')
                if(toggle){
                    href = toggle.attr('href');
                    toggle.attr({
                        'data-href': href
                    })[0].removeAttribute('href');

                    toggle[0].addEventListener('click', toogleUi.click);
                    toggle.find('span').html(toggle.attr('data-min-title'));

                    toggle[0].removeEventListener('mouseover', toogleUi.mouseover);
                    toggle[0].removeEventListener('mouseleave', toogleUi.mouseleave);

                }
                if(dropdown.category){
                    dropdown.category.find('[data-href]').each(function(){
                        $(this)[0].addEventListener('click', ui.click);
                        $(this)[0].removeEventListener('mouseover', ui.mouseover);
                        $(this)[0].removeEventListener('mouseleave', ui.mouseleave);
                    });
                    popup[0].removeEventListener('mouseover', function () {
                        clearTimeout(delay);
                    });
                    popup[0].removeEventListener('mouseleave', ui.mouseleave);

                    dropdown.element[0].removeEventListener('mouseover', toogleUi.clear);
                    dropdown.element[0].removeEventListener('mouseleave', toogleUi.mouseleave);
                }
            }

            return window.innerWidth;
        }

        function start() {
            dropdown.element = menu.find('[data-menu=dropdown]') || false;
            toggle = menu.find('[data-menu=toggle]').append('<span></span>') || false;
            dropdown.category = menu.find('[data-menu=category]') || false;
            header = menu.find('[data-menu=header]') || false;
            popup = menu.find('[data-menu=popup]') || false;
            menu.find('[data-menu=category-toggle]').on('touched click', function () {
                if(dropdown.category){
                    if(dropdown.category.is(":visible")){
                        dropdown.category.slideUp(400);
                        $(this).removeClass('in-active');
                    }else{
                        dropdown.category.slideDown(400);
                        $(this).addClass('in-active');
                    }
                }
            })


            if(dropdown.category && popup){
                dropdown.category.find('.menu-category-data').each(function (i, index) {
                    key = Math.random().toString(36).substring(7);
                    clone.push($(this).clone());
                    $(this)
                        .attr('id', key)
                        .closest('li')
                        .find('.menu-category-item')
                        .attr({
                            'data-href-index' : i,
                            'data-href' : key
                        });
                    popup.append(clone[i].css({'display':'none'}));
                });
            }


            window.addEventListener('resize', function () {
                clearTimeout(update);
                update = setTimeout(function () {
                    if(wv != window.innerWidth){
                        wv = layout();
                    }
                }, 300)
            });

            $(window).on('load', function () {
                layout();
                menu.removeClass('running');
            })
        }
        start();
    }

    $.fn.search = function (option) {
        var config = $.extend({
            callback : false
        }, option);

        var self = $(this);
        var button = $(this).find('[data-search=button]') || false;
        var field = $(this).find('[data-search=control]') || false;
        var dropdown = $(this).find('[data-search=dropdown]').css({'display':'none'}) || false;

        function submit() {
            // тут общий сабмит для формы
            self.submit();
        }



        if(button){
            button.on('click', function () {
                if($(this).css('content') == '"mobile"' || $(this).css('content') == 'mobile'){
                    if(!self.hasClass('opened')){
                        self.addClass('opened')
                        field.focus();
                    }else if(field.val() == ''){
                        self.removeClass('opened');
                        dropdown.fadeOut(300);
                    }else{
                        submit();
                    }
                }else if(field.val() != ''){
                    submit();
                }
            });
        }
        if(field){
            field.on('click keyup write paste', function(e) {
                val = $(this).val();
                if(val.length > 3){
                    console.log(val, 'debug');
                    // тут запрос по значению
                    // тестовый пример
                    $.ajax({
                        type : 'POST',
                        url : field.attr('data-url'),
                        success: function (result) {
                            if(result){
                                dropdown.html(result).fadeIn(300)
                            }else{
                                dropdown.fadeOut(300);
                            }
                        }
                    })
                }else{
                    dropdown.fadeOut(300);
                }
            }).trigger('write');

            $(document).on('click', function(e){
                if (!self.is(e.target) && self.has(e.target).length === 0) {
                    if(dropdown.is(":visible") || self.hasClass('opened')) {
                        self.removeClass('opened');
                        field.val('');
                        dropdown.fadeOut(300);
                    }
                }
            });
        }
        window.addEventListener( "pageshow", function ( event ) {
            var historyTraversal = event.persisted ||
                ( typeof window.performance != "undefined" &&
                    window.performance.navigation.type === 2 );
            if ( historyTraversal ) {
                window.location.reload();
            }
        });

    }
})(jQuery);