

$.fn.mapGeo = function(geo, option){
    var json = geo;
    var self = $(this), prev = false;
    var mapElement = $(this).get(0);
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer;
    var config = $.extend({
        icon: 'images/css/location.png',
        iconActive: 'images/css/location-in.png',
        toggle: $('[data-role=locate-change]')
    }, option);

    var bubles = [], markers = [];

    var mapOptions = {
        zoom: 15,
        scrollwheel: false,
        center: new google.maps.LatLng(55.797720, 49.109656)//,
      //  styles :  [{"featureType":"administrative","stylers":[{"visibility":"off"}]},{"featureType":"poi","stylers":[{"visibility":"simplified"}]},{"featureType":"road","stylers":[{"visibility":"simplified"}]},{"featureType":"water","stylers":[{"visibility":"simplified"}]},{"featureType":"transit","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"visibility":"off"}]},{"featureType":"road.local","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"water","stylers":[{"color":"#84afa3"},{"lightness":52}]},{"stylers":[{"saturation":-77}]},{"featureType":"road"}]
    };
   

    var bounds;

    if(json.length == 1){
        $.extend(mapOptions,{
            center: new google.maps.LatLng(json[0].lat, json[0].lng)
        })
    }else if(json.length != 0){
        bounds = new google.maps.LatLngBounds();
    }
    var map = new google.maps.Map(mapElement, mapOptions);
    directionsDisplay.setMap(map);

    function iconRender(path){
        //return new google.maps.MarkerImage(path, new google.maps.Size(24, 34), new google.maps.Point(0, 0), new google.maps.Point(11, 36))
        return new google.maps.MarkerImage(path, new google.maps.Size(32, 45), new google.maps.Point(0, 0), new google.maps.Point(45, 0))
    }
    for(i=0;i<json.length;i++){
        bubles.push(
            new InfoBubble({
                map: map,
                content: '<div class="js-bubble-container"><div onclick="$(document).trigger(\'bubles.close\')" class="js-bubble-close icon-close"></div>'+json[i].html+'</div>',
                position: new google.maps.LatLng(json[i].lat, json[i].lng),
                shadowStyle: 0,
                padding: 0,
                maxWidth: 280,
                minWidth: 280,
                borderRadius: 3,
                disableAnimation:true,
                arrowStyle: 1,
                autopanMargin: 1,
                arrowSize: 10,
                borderWidth: 0,
                maxHeight: 350,
                minHeight: 20,
               // height: 'auto',
                arrowPosition: 5
            })
        );

        function changeM(marker, buble){
            if(prev){
                prev.buble.close(map);
                prev.marker.setIcon(iconRender(config.icon))
            }
            if(marker && buble){
                map.setCenter(marker.getPosition());
                marker.setIcon(iconRender(config.iconActive));
                buble.open(map);
                return {'marker': marker, 'buble': buble};
            }
        }
       
        var marker = new google.maps.Marker({
                position: { lat : json[i].lat, lng: json[i].lng},
                map: map,
                icon: iconRender(config.icon)
        }).addListener('click', function() {
            for(i=0;i<bubles.length;i++){
                if(bubles[i].position.lat() == this.getPosition().lat() && bubles[i].position.lng() == this.getPosition().lng()){
                    prev = changeM(this, bubles[i])
                }
            }
        });

        $(document).on('bubles.close', function(){
            changeM(false, false);
        });

        if(json.length > 1){
            bounds.extend(new google.maps.LatLng(json[i].lat, json[i].lng));
        }
    }
    if(json.length > 1){
        map.fitBounds(bounds);
    }

    self.on('resize', function () {
        map.fitBounds(bounds);
    });


    function calculateAndDisplayRoute(directionsService, directionsDisplay, current, destination) {
        directionsService.route({
            origin: { lat: current.coords.latitude, lng: current.coords.longitude},
            destination: destination,
            //  waypoints: waypts,
            optimizeWaypoints: true,
            travelMode: google.maps.TravelMode.DRIVING
        }, function(response, status) {
            if (status === google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
                var route = response.routes[0];

                $('body, html').animate({
                    scrollTop:self.offset().top
                }, 600);

            } else {
                window.alert('Не удалось определить Ваше местоположение!');
            }
        });
    }
    function getRoute(elat, elng) {
        if(!!navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                calculateAndDisplayRoute(directionsService, directionsDisplay, position, {lat:elat, lng:elng});
            })
        }
    }

    $(document).on('touched click', '[data-role=get-route]', function(){
        getRoute(Number($(this).data('lat')),Number($(this).data('lng')) )
    });

    $(document).on('get-route-target', function (e, r) {
        getRoute(r.lat, r.lng)
    })

}




